package study.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 2021/10/12
 *
 * @author kun.li03
 */
public class SLF4J2Log4j2Simulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(SLF4J2Log4j2Simulator.class);

    public static void main(String[] args) {
        LOGGER.info("I am a log record...");
    }
}
