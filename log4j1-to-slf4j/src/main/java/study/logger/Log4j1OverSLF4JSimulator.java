package study.logger;

import org.apache.log4j.Logger;

/**
 * 2021/10/12
 *
 * @author kun.li03
 */
public class Log4j1OverSLF4JSimulator {

    private static final Logger LOGGER = Logger.getLogger(Log4j1OverSLF4JSimulator.class);

    public static void main(String[] args) {
        LOGGER.info("I am a log record...");
    }
}
