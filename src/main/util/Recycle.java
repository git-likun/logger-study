package util;

import java.util.concurrent.TimeUnit;

/**
 * 2021/10/11
 *
 * @author kun.li03
 */
public class Recycle {
    private int sec;
    private Runnable runnable;

    public Recycle(int seconds, Runnable runnable) {
        this.sec = seconds;
        this.runnable = runnable;
    }

    public void cycle() {
        try {
            while (true) {
                TimeUnit.SECONDS.sleep(sec);
                this.runnable.run();
            }
        } catch (Exception ex) {
            throw new RuntimeException();
        }

    }

}
