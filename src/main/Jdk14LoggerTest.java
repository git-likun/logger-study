
import java.util.logging.Logger;

/**
 * 2021/10/7
 *
 * @author kun.li03
 */
public class Jdk14LoggerTest {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger(Jdk14LoggerTest.class.getName());

        logger.warning("Send warn info");
        logger.info("Logger info level info  ");
    }
}
