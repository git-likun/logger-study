import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
//import org.slf4j.impl.StaticBinder;

/**
 * 2021/9/24
 *
 * @author kun.li03
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        while (true) {
            TimeUnit.SECONDS.sleep(1);
            LOGGER.info("Print " + System.currentTimeMillis());
        }
    }
}
