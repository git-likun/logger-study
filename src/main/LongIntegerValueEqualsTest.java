import java.util.Objects;

/**
 * 2021/10/12
 *
 * @author kun.li03
 */
public class LongIntegerValueEqualsTest {
    public static void main(String[] args) {
        System.out.println(Objects.equals(new Integer(1), 1L));
    }
}
