package study.logger;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Recycle;

/**
 * 2021/10/11
 *
 * @author kun.li03
 */
public class DynamicLogbackConfigTest {

//    private static final Logger LOGGER = LoggerFactory.getLogger("study.logger");

    private static final Logger LOGGER = LoggerFactory.getLogger(Thread.currentThread().getClass().getName());
    static {
        ch.qos.logback.classic.Logger logger =  (ch.qos.logback.classic.Logger)LOGGER;
    }

    public static void main(String[] args) {
        new Recycle(2, () -> LOGGER.debug("do somthing")).cycle();
    }
}
