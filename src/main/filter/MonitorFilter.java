package filter;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggerContextAware;
import ch.qos.logback.classic.spi.LoggerContextVO;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 2021/10/7
 *
 * @author kun.li03
 */
public class MonitorFilter extends Filter<ILoggingEvent> implements LoggerContextAware {

    AtomicInteger counter = new AtomicInteger(0);

    public FilterReply decide(ILoggingEvent event) {
        event.getLoggerContextVO().getName();
        System.out.println(counter.getAndIncrement());
        getContext();
        return FilterReply.ACCEPT;
    }

    public void setLoggerContext(LoggerContext context) {

    }
}
