package filter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.turbo.TurboFilter;
import ch.qos.logback.core.spi.FilterReply;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 2021/10/7
 *
 * @author kun.li03
 */
public class SampleTurboFilter extends TurboFilter {

    String marker;
    Marker markerToAccept;
    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public FilterReply decide(Marker marker, Logger logger, Level level,
                              String format, Object[] params, Throwable t) {

        System.out.println(logger.getName() + " " + counter.incrementAndGet());;

        if (!isStarted()) {
            return FilterReply.NEUTRAL;
        }

        if ((markerToAccept.equals(marker))) {
            return FilterReply.ACCEPT;
        } else {
            return FilterReply.NEUTRAL;
        }
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String markerStr) {
        this.marker = markerStr;
    }

    @Override
    public void start() {
        if (marker != null && marker.trim().length() > 0) {
            markerToAccept = MarkerFactory.getMarker(marker);
            super.start();
        }
    }
}
