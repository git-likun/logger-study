package study.logger;

import org.apache.log4j.Logger;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.slf4j.impl.StaticLoggerBinder;

import java.util.logging.LogManager;

/**
 * 2021/10/12
 *
 * @author kun.li03
 */
public class ApplicationSimulator {

    private static final Logger LOGGER = Logger.getLogger(ApplicationSimulator.class);

    public static void main(String[] args) {
        LOGGER.info("I am a log4j log...");
    }
}
