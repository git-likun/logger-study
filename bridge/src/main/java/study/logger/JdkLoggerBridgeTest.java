package study.logger;

import org.slf4j.bridge.SLF4JBridgeHandler;

/**
 * 2021/10/15
 *
 * @author kun.li03
 */
public class JdkLoggerBridgeTest {

    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(ApplicationSimulator.class.getName());

    static {
        LOGGER.info("this is one log info ...");
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        LOGGER.info("this is one log info ...");
    }

    public static void main(String[] args) {
    }
}
